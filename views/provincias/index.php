<?php

use app\models\Provincias;
use yii\helpers\Html;
use yii\helpers\Url;
use yii\grid\ActionColumn;
use yii\grid\GridView;

/** @var yii\web\View $this */
/** @var yii\data\ActiveDataProvider $dataProvider */

$this->title = 'Provincias';
$this->params['breadcrumbs'][] = $this->title;
?>
<div class="provincias-index">

    <h1><?= Html::encode($this->title) ?></h1>

    <p>
        <?= Html::a('Añadir provincias', ['create'], ['class' => 'btn btn-success']) ?>
    </p>


    <?= GridView::widget([
        'dataProvider' => $dataProvider,
        'columns' => [
         //   ['class' => 'yii\grid\SerialColumn'],

            'autonomia',
            'provincia',
            'poblacion',
            'superficie',
            [
                'class' => ActionColumn::className(),
                'urlCreator' => function ($action, Provincias $model, $key, $index, $column) {
                    return Url::toRoute([$action, 'provincia' => $model->provincia]);
                 }
            ],
        ],
    ]); ?>


</div>
